
from flask_restful import Api, Resource, reqparse
from models import Quiz, StudentAnswers, GameEmotions, EMOTION_GAME_LIST, Question
import numpy as np
from flask import jsonify, request
import json
from bson.objectid import ObjectId
from random import shuffle
import os
import hashlib
from collections import Counter

quiz_parser = reqparse.RequestParser()
quiz_parser.add_argument('quiz', type=dict)
question_parser = reqparse.RequestParser()
question_parser.add_argument('questions', type=dict)
answer_parser = reqparse.RequestParser()
answer_parser.add_argument('answers', type=dict)
game_parser = reqparse.RequestParser()
game_parser.add_argument('stats', type=dict)
analysis_parser = reqparse.RequestParser()
analysis_parser.add_argument('analytics', type=dict)


def calc_ocean_results(ocean_answers):
    results = {}
    max_score = {}
    min_score = {}
    for r in ocean_answers.answers:
        # Get the category for the question type
        category = r.question.question_type.category
        # Get the weights for the OCEAN model
        selection_weights = [
            int(i)
            for i in r.question.question_type.options.keys()
        ]
        if category not in results.keys():
            results[category] = selection_weights[r.value - 1]
            # Adds the minimum and maximum limits to the score
            max_score[category] = max(selection_weights)
            min_score[category] = min(selection_weights)
        else:
            results[category] += selection_weights[r.value - 1]
            # Adds the minimum and maximum limits to the score
            max_score[category] += max(selection_weights)
            min_score[category] += min(selection_weights)
    return results, min_score, max_score


def create_url(quiz, user_id):
    full_url = os.path.join(
        "http://" + os.environ["DOMAIN"],
        quiz.post_page
    )
    if quiz.phase == "PRE" and quiz.game != "ocean":
        full_url += "?user_id=" + str(user_id)
        next_q = Quiz.objects(phase="POST", game__iexact=quiz.game).first()
        full_url += "&post_url={0}".format(create_url(next_q, user_id))
    elif quiz.game == "ocean":
        full_url = os.path.join(full_url, str(user_id))
    elif quiz.phase == "POST":
        full_url = os.path.join(full_url, "quiz", str(quiz.id))
    return full_url


class EmailAPI(Resource):
    def post(self, user_id):
        email = email_parser.parse_args()
        try:
            msg = Message(
                """Olá!\n Você está recebendo esse email devido ao Projeto de Computação Afetiva da Universidade Estadual de Campinas (UNICAMP).\n Seu código de acesso é {0} na página {1}
                """.format(user_id, os.environ.get("DOMAIN")),
                recipients=[email["email"]])
            mail.send(msg)
            return '', 200
        except Exception as e:
            return jsonify(e), 500


class GameEmotionsAPI(Resource):
    def post(self):
        args = game_parser.parse_args()
        try:
            args['stats']['emotions'] = [
                EMOTION_GAME_LIST[int(i)]
                for i in args['stats']['emotions']
            ]
            new_game = GameEmotions(**args["stats"])
            new_game.save()
            return '', 200
        except Exception as e:
            return jsonify(e), 500


class StudentAnswersAPI(Resource):
    def get(self, user_id):
        answers = StudentAnswers.objects(user=user_id)
        return jsonify(answers)

    def post(self, user_id):
        args = answer_parser.parse_args()
        try:
            new_quiz = StudentAnswers(
                user=user_id, answers=args['answers']['answers'], quiz=args['answers']['quiz'])
            new_quiz.save()
            return '', 201
        except Exception as e:
            return jsonify(e), 500


class StudentAnalysisAPI(Resource):
    def post(self):
        args = analysis_parser.parse_args()
        default_emotions = {i: 0 for i in EMOTION_GAME_LIST}
        # Make a summarization of all data
        data = {"answers": []}
        # Get all emotions from the games
        all_emotions = GameEmotions.objects().order_by(
            '-created_at')
        game_emotions = {}
        for em in all_emotions:
            game_name = em.game
            if "agnus" in em.game:
                game_name = "agnus"
            if game_name not in game_emotions.keys():
                game_emotions[game_name] = {}

            if game_name == "animanomicon":
                limit = 3
            else:
                limit = 7

            game_emotions[game_name][str(
                em.user)] = em.emotions[:limit]
        # Parse files looking for answers
        ground_truth = {}
        for truth in args["analytics"]["answer_sheets"]:
            ground_truth[truth["game"]] = {}
            for t in truth["sheet"]:
                hashing = hashlib.md5(
                    t["question"].encode('utf-8')).hexdigest()
                ground_truth[truth["game"]][hashing] = int(t["value"])
        # Calculates all OCEAN personality traits
        ocean_quiz = Quiz.objects(game__iexact="ocean").only("id")
        ocean = StudentAnswers.objects(quiz__in=ocean_quiz).order_by(
            '-created_at')
        ocean_results = {}
        for o in ocean:
            ocean_results[str(o.user)], min_score, max_score = calc_ocean_results(
                o)
            # Scales the ocean results to a percentage relative to the score
            ocean_results[str(o.user)] = {
                k: 100 * ((ocean_results[str(o.user)][k] - min_score[k]) /
                          (max_score[k] - min_score[k]))
                for k in ocean_results[str(o.user)].keys()
            }

        # Get the test scores for each user
        tests = StudentAnswers.objects(quiz__nin=ocean_quiz).order_by(
            *['user', 'game', 'phase'])
        # Get the pre test scores for all users
        pre_eval = {}
        def get_key(x, y): return str(x) + "_" + str(y)
        for t in tests:
            if t.quiz.phase == "PRE":
                k = get_key(t.user, t.quiz.game)
                if k not in pre_eval.keys():
                    pre_eval[k] = {}
                for a in t.answers:
                    q_id = str(a.question.id)
                    pre_eval[k][q_id] = a.value

        for t in tests:
            if t.quiz.phase == "POST":
                sheet = {"user": str(t.user), "quiz_title": t.quiz.title,
                         "game_title": t.quiz.game}
                game = t.quiz.game
                # Appends right or wrong answers
                right_answers = 0
                total_answers = 0
                for a in t.answers:
                    q_text = a.question.text
                    if game in ground_truth.keys():
                        hex_text = hashlib.md5(
                            q_text.encode('utf-8')).hexdigest()
                        if hex_text in ground_truth[game].keys():
                            if ground_truth[game][hex_text] == a.value:
                                right_answers += 1
                            total_answers += 1
                        elif a.text != None and a.text != "":
                            sheet[a.question.text] = a.text
                k = get_key(t.user, t.quiz.game)
                # This assumes that all answers have 5 options in the PRE quiz
                norm_eval = [(pre_eval[k][i] - 1) /
                             4 for i in pre_eval[k].keys()]
                sheet["mean_pre_percentage"] = np.mean(norm_eval)
                sheet["pre_value"] = pre_eval[k]
                if total_answers > 0:
                    sheet["post_score_percentage"] = right_answers / \
                        total_answers
                # Appends OCEAN traits
                if str(t.user) in ocean_results.keys():
                    sheet.update(ocean_results[str(t.user)])
                # Appends perceptual dominant emotion
                if game in game_emotions.keys():
                    print(game_emotions[game])
                    print(game, str(t.user) in game_emotions[game].keys())
                    sheet["dominant_emotion"] = Counter(
                        game_emotions[game][str(t.user)]).most_common(1)[0][0]
                    sheet["gameplay_emotions"] = game_emotions[game][str(
                        t.user)]
                data["answers"].append(sheet)
        return jsonify(data)


class QuizList(Resource):
    def get(self, user_id=None):
        phase = request.args.get("phase")
        if user_id is None:
            if phase is not None:
                quizzes = Quiz.objects.filter(
                    phase=phase)
                answers = []
            else:
                quizzes = Quiz.objects()
                answers = []

            user_id = str(ObjectId())
            data = {"user_id": user_id}
        else:
            # Returns the unanswered quizzes and the answered ones
            answers = StudentAnswers.objects(user=user_id)
            answers_id = list(set([
                a.quiz.id
                for a in answers
            ]))
            if phase is not None:
                quizzes = Quiz.objects(
                    id__nin=answers_id, phase=phase)
            else:
                quizzes = Quiz.objects(id__nin=answers_id)
            data = {"user_id": user_id}

        data["answers"] = [{
            "id": str(a.id),
            "user_id": user_id,
            "quiz_name": a.quiz.title,
        } for a in answers]

        data["quizzes"] = [{
            "id": str(q.id),
            "language": q.language,
            "description": q.description,
            "title": q.title,
            "phase": q.phase,
            "game": q.game,
            "post_page": create_url(q, user_id),
            "questions": q.questions
        } for q in quizzes]

        # randomize show order
        shuffle(data["quizzes"])
        return jsonify(data)


class QuizAPI(Resource):
    def get(self, id_value=None):

        user_id = request.args.get("user_id")
        q = Quiz.objects.get(id=id_value)
        data = {}
        data["quiz"] = {
            "id": str(q.id),
            "language": q.language,
            "description": q.description,
            "title": q.title,
            "phase": q.phase,
            "game": q.game,
            "post_page": create_url(q, user_id),
            "questions": q.questions
        }
        return jsonify(data)

    def delete(self, id_value):
        pass

    def post(self):
        args = quiz_parser.parse_args()
        try:
            new_quiz = Quiz(**args["quiz"])
            new_quiz.save()
            return '', 201
        except Exception as e:
            return jsonify(e), 500

    def put(self, id_value):
        args = quiz_parser.parse_args()
        changed_object = Quiz.objects.update_one(
            args["quiz"], upsert=True, full_result=True)
        return jsonify(changed_object), 201
