### Planktons

https://www.youtube.com/watch?v=xFQ_fO2D7f0

http://www.io.usp.br/index.php/infraestrutura/museu-oceanografico/29-portugues/publicacoes/series-divulgacao/vida-e-biodiversidade/809-plancton

1. 1. O termo "plankton" vêm da palavra grega "planktos", que significa:

   * vaguear
   * flutuando
   * inumerável
   * transparente

2. O que distingue um fitoplâncton de outros tipos de plânctons?
3. Por quê os fitoplânctons são importantes?
4. Quão abundantes são os plânctons?
   * O tamanho de todos os plânctons seria igual ao tamanho do Burj Khalifa (maior prédio do mundo)
   * Se você alinhasse todos os plânctons da terra, eles se estenderiam até a lua
   * Eles são metade de toda a biomassa presente no nosso planeta
   * Uma simples colher de chá de água do mar pode conter mais de um milhão de plânctons
5. O fitoplâncton é formado por organismos microscópicos autotróficos que vivem em suspensão no ambiente aquático. Marque a alternativa que explica corretamente por que esses organismos são encontrados apenas na zona eufótica em ambientes marinhos.
   * O fitoplâncton só ocorre na zona eufótica, pois é a única região em que há as correntes de água.
   * O fitoplâncton só ocorre na zona eufótica, pois é essa a região onde ele consegue capturar seu alimento.
   * O fitoplâncton só ocorre na zona eufótica, pois nesse local a pressão permite que ele mantenha-se em suspensão.
   * O fitoplâncton só ocorre na zona eufótica, pois nessa área ele possui disponibilidade de luz.
6. Qual tipo de emoção predominou durante a exibição do vídeo e leitura do texto?