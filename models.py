from mongoengine import *
import datetime as dt
from bson.objectid import ObjectId

EMOTION_GAME_LIST = [
    'neutral', 'happy', 'fear', 'anger'
]


def _key_is_number(val):
    valid = True
    for k in val.keys():
        try:
            float(k)
        except ValueError:
            valid = False
            break
    if not valid:
        raise ValidationError(
            'answer key has to be a number - either positive or negative')


class QuestionType(EmbeddedDocument):
    category = StringField(max_length=60, required=True)
    options = DictField(validation=_key_is_number)

    @staticmethod
    def is_textfield(self):
        if self.options is {}:
            return True
        else:
            return False


class Question(Document):
    created_at = DateTimeField(default=dt.datetime.utcnow)
    text = StringField(max_length=1800, required=True)
    question_type = EmbeddedDocumentField(QuestionType,
                                          required=False)


class Language(EmbeddedDocument):
    name = StringField(max_length=200, required=True)
    code = StringField(max_length=20, required=True)


class Quiz(Document):
    created_at = DateTimeField(default=dt.datetime.utcnow)
    title = StringField(max_length=120, required=True, unique=True)
    language = EmbeddedDocumentField(Language)
    game = StringField(max_length=200, required=True)
    description = StringField(max_length=600)
    questions = ListField(ReferenceField(Question, required=True))
    post_page = StringField(max_length=200, default="")
    phase = StringField(required=True, choices=["PRE", "POST"], default="PRE")


class Response(EmbeddedDocument):
    created_at = DateTimeField(default=dt.datetime.utcnow)
    question = ReferenceField(Question)
    value = IntField(required=False)
    text = StringField(max_length=200, required=False)


class StudentAnswers(Document):
    created_at = DateTimeField(default=dt.datetime.utcnow)
    quiz = ReferenceField(Quiz)
    answers = EmbeddedDocumentListField(Response)
    user = ObjectIdField(required=True, default=ObjectId)


class GameEmotions(Document):
    created_at = DateTimeField(default=dt.datetime.utcnow)
    game = StringField(max_length=200, required=True)
    emotions = ListField(StringField(required=True))
    elapsed_time = FloatField(min_value=0.0)
    user = ObjectIdField(required=True)
