
import glob
import os
import json
from pathlib import Path
from models import Quiz, Question, Language


def create_language(filename):
    lang_code = str(Path(filename).parent).split("/")[-1]
    lang = {"code": lang_code.strip().lower(), "name": ""}
    if lang_code == "pt-br":
        lang["name"] = "Portugues Brasileiro"
    elif lang_code == "en":
        lang["name"] = "English"

    return Language(**lang)


def load_big_five():
    n_quizzes = Quiz.objects().count()
    if n_quizzes <= 0:
        # Load model into MongoDB
        folder = os.path.dirname(os.path.abspath(__file__))
        root_folder = Path(folder).parent
        quiz_folder = os.path.join(root_folder, 'quizzes')
        for filename in glob.glob(quiz_folder + '/**/*.json', recursive=True):
            # Creates the language if it does not exist yet
            lang = create_language(filename)
            print(filename)
            # Loads the quiz into the database
            with open(filename, 'r') as f:
                quiz = json.load(f)
                existing_quiz = Quiz.objects(
                    language__code=lang['code'], title=quiz["title"]).count()
                if existing_quiz <= 0:
                    questions = []
                    for q in quiz['questions']:
                        new_q = Question(**q)
                        new_q.save()
                        questions.append(new_q)
                    quiz['questions'] = questions
                    Quiz(language=lang, **quiz).save()
