"""Classes and libraries for using 12 Factor App guidelines"""
import logging
import os
import sys
from pathlib import Path
# Use the built-in version of scandir/walk if possible, otherwise
# use the scandir module version
try:
    from os import scandir, walk
except ImportError:
    from scandir import scandir, walk

logger = logging.getLogger(__file__)


class EnvReader():
    def __init__(self, fpath=None, delim="=", *args, **kwargs):
        self._read_file(fpath=fpath, delim=delim)
        super().__init__(*args, **kwargs)

    def __find_envfile(self, path=None):
        """Yield file names ending with '.env' under given path."""
        for entry in os.scandir(path):
            if entry.name.endswith('.env') and entry.is_file():
                yield entry.path

    def _read_file(self, fpath=None, delim="="):
        print(fpath)
        if fpath is None:
            # Gets the root folder of the project
            folder = os.path.dirname(os.path.abspath(__file__))
            root_folder = Path(folder).parent
            # Searches for a .env file
            fpath = next(self.__find_envfile(root_folder))

        try:
            with open(fpath) as f:
                logger.info("[ENV] Reading Env file...")
                for line in f.readlines():
                    # Splits the parameter based on the delimiter
                    param, value = line.strip().split(delim)
                    logger.info(
                        "[ENV] Exported variable {0} with value {1}".format(param, value))
                    os.environ[param] = value

        except FileNotFoundError:
            logger.error("[ENV] Env File not found at {0}".format(fpath))

    def get_env(self, env_name="PYTHON_ENV"):
        param = os.environ.get(env_name, None)
        if param is not None:
            # Counts the number of dots on string
            dots = param.count(".")
            if dots <= 0:
                # Either value is string or int
                try:
                    value = int(param)
                except ValueError:
                    value = str(param)
            elif dots == 1:
                # Tries to parse it to float
                try:
                    value = float(param)
                except ValueError:
                    value = str(param)

            else:
                # It is definitely a string
                value = str(param)
            return value
        else:
            logger.warn("[ENV] Env variable {0} not found!".format(env_name))
            return None
