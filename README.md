# Psichological and Emotional influences in Learning

## Running and installing dependencies

This package uses pipenv as the main dependency manager. To install it, run the following command:

```console
pip install pipenv
```


To run the code, cd into this package and run the following commands:

```console
pipenv shell
python server.py
```

This package also assumes that there is a .env file in the root project, with the following config variables

```python
PYTHON_ENV=development # python environment, either prodution or development
HOST=localhost # flask host endpoints
PORT=5000 # flask api port
MONGODB_DBNAME=test # Mongodb name
MONGODB_URI=mongodb://localhost/test # Connection uri
```

## API Endpoints


Register new user
```console
curl -d "
    {
      "email": "teste@teste.com,
      "password": 123
    }" -X POST localhost:5000/register
```

Login using JWT authentication

```console
curl -d "
    {
      "email": "teste@teste.com,
      "password": 123
    }" -X POST localhost:5000/login
```


Get all available quizzes:

```console
curl -X GET "localhost:5000/quizzes"
```

Create a new quiz:

```console
curl -d "
    {"quiz": {
    "language": {
      "code": "pt-br",
      "name": "Portugues Brasileiro"
    },
    "questions": [
    {
        "answer": {
          "category": "CTA",
          "options": {
            "1": "OK",
            "2": "NOK"
          }
        }
    }
    ]}}" -X POST localhost:5000/quizzes
```

Get all available answers (requires auth):

```console
curl -X GET "localhost:5000/answers"
```

Create a new answer to a quiz (requires auth):

```console
curl -d "{
    "answers": {
      "quiz": "quiz_id",
      "answers": [
      {
          "question": "123",
          "value": 1
      },
      {
          "question": "123",
          "text": "This is the answer"
      }
    ]}}" -X POST localhost:5000/answers
```

Create a new game stat:

```console
curl -d "{
  "stats": {
    "emotions": [0,1,1,1],
    "user":"5dc61ab55cd6b06a6ad6e268",
    "game": "untitled"
  }
}" -X POST localhost:5000/game_stats
```
