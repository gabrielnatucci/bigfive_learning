"""Handles the webserver start and port bindings"""
from flask import Flask, jsonify, request, render_template, redirect, url_for
from flask_restful import Api
from flask_cors import CORS
from flask_mail import Mail, Message
from flask_mongoengine import MongoEngine
from api import QuizAPI, QuizList, StudentAnswersAPI, GameEmotionsAPI, StudentAnalysisAPI, calc_ocean_results
from models import StudentAnswers, Quiz
from helpers import twelve_factor
import os

app = Flask(__name__,
            static_url_path='',
            static_folder='web',
            template_folder='templates')

CORS(app)
# Loads a .env file
env = twelve_factor.EnvReader()

# Changes the MONGODB uri settings based on .env files
app.config['MONGODB_SETTINGS'] = {
    'db': env.get_env("MONGODB_DBNAME"),
    'host': env.get_env("MONGODB_URI")
}
app.config['SECRET_KEY'] = os.urandom(12).hex()
app.config['MAIL_SERVER'] = env.get_env("MAIL_SERVER")
app.config['MAIL_PORT'] = env.get_env("MAIL_PORT")
app.config['MAIL_USERNAME'] = env.get_env("MAIL_USERNAME")
app.config['MAIL_PASSWORD'] = env.get_env("MAIL_PASSWORD")
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_DEFAULT_SENDER'] = env.get_env("MAIL_DEFAULT_SENDER")
app.config['MAIL_SUPPRESS_SEND'] = False

mail = Mail(app)
api = Api(app, prefix="/api")

db = MongoEngine(app)


# declare all api specifications
api.add_resource(StudentAnswersAPI, '/answers/<string:user_id>')
api.add_resource(QuizAPI, '/quiz/<string:id_value>')
api.add_resource(QuizList, '/quizzes/<string:user_id>', '/quizzes')
api.add_resource(GameEmotionsAPI, '/game_stats')
api.add_resource(StudentAnalysisAPI, '/analysis')


@app.route("/api/plankton", methods=["GET"])
def render_plankton():
    post_quiz_plankton = Quiz.objects(
        phase="POST", game__iexact="plankton").first()
    return_url = os.path.join(
        "http://", os.environ["DOMAIN"], "quiz", str(post_quiz_plankton.id))

    user_id = request.args.get("user_id")
    if user_id is not None:
        return_url += "?user_id=" + str(user_id)
    return render_template("plankton.html", return_url=return_url)


@app.route("/api/email/<user_id>",  methods=['POST'])
def email_user(user_id):
    try:
        data = request.json
        html = """<p>Olá! Você está recebendo esse email devido ao Projeto de Computação Afetiva da Universidade Estadual de Campinas (UNICAMP).<br/> Seu código de acesso é <b>{0}</b> na nossa <a href='http://{1}/'>página</a>.</p>
        <p>
        Lembramos que nenhuma informação pessoal será gravada (nem mesmo seu email para contato!), e seus dados serão utilizados única e exclusivamente no contexto da disciplina IA369Y - Computação Afetiva, ministrada no segundo semestre de 2019.
        </p>
        <p>Nosso objetivo é estudar e pesquisar a relação entre personalidade e emoção e analisar como esses fatores afetam o aprendizado de novos conceitos. Contamos (e muito!) com você para isso =)</p>
        <p>
        Muito obrigado pelo suporte!!!
        <ul style="list-style-type:none">
            <li> Gabriel C. Natucci </li>
            <li>Marcelo Rigon</li>
            <li>Nathan Ribeiro</li>
            <li><b>Professora Responsável:</b> Profa. Dra. Paulo Dornhofer Paro Costa</li>
        </ul>
        </p>
        """.format(user_id, os.environ.get("DOMAIN"))
        msg = Message(subject="[UNICAMP] Acesso ao sistema teste de Disciplina Jogos, Psicologia e Computação Afetiva",
                      recipients=[data["email"]])
        msg.html = html
        mail.send(msg)
        return '', 200
    except Exception as e:
        raise
        return "", 500


@app.route('/api/ocean/result/<string:user_id>', methods=["GET"])
def personality_result(user_id):
    user_results = StudentAnswers.objects(user=user_id).order_by('-created_at')
    if user_results is not None:
        ocean_results = [r for r in user_results
                         if "personalidade" in r.quiz.title.lower()]
        if len(ocean_results) > 0:
            res = ocean_results[0]
            # Sums up all answers by its category
            results, min_score, max_score = calc_ocean_results(res)
            response = {}
            for k in results.keys():
                response[k] = {
                    "user_score": results[k],
                    "min_score": min_score[k],
                    "max_score": max_score[k]
                }
            return jsonify(response)

        else:
            return "No OCEAN Personality test found for id {0}".format(user_id), 200
    else:
        return "No user id provided!", 500


if __name__ == "__main__":
    from gevent.pywsgi import WSGIServer
    from helpers import data_loaders
    # Sets the hosting parameters
    host = env.get_env("HOST")
    port = env.get_env("PORT")
    os.environ["HOST"] = host
    os.environ["PORT"] = str(port)
    # Sets the initial quizzes
    data_loaders.load_big_five()

    if env.get_env("PYTHON_ENV") == "production":
        http_server = WSGIServer((host, port), app)
        http_server.serve_forever()
    else:
        app.run(host=host, port=port, debug=True)
